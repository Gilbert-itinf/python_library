from Book_Class import Books
from CD_Class import CD
from Movie_Class import Movies
import pickle

def create_standard_library(library):
    b2 = Books("Harry Potter", "J.K Rowling", 1200, 350, 2008,)
    b = Books("Sagan om Ringen", "J.R.R Tolken", 859, 400, 2016,)
    b1 = Books("Allt om Mat", "Gibbs Andersson", 94, 280, 2021,)
    c = CD("Hybrid Theory", "Linkin Park", 12, 109, 2007)
    c1 = CD("Absolute Kidz 4", "Random", 15, 130, 2001)
    c2 = CD("Absolute Kidz 4", "Random", 15, 130, 2001)
    m = Movies("Fightclub", "David Fincher", 139, 150, 2010)
    m1 = Movies("Aaa1", "GD", 200, 200, 2020)
    
    library['books'].append(b2)
    library['books'].append(b1)
    library['books'].append(b)
    library['cds'].append(c1)
    library['cds'].append(c)
    library['cds'].append(c2)
    library['movies'].append(m)
    library['movies'].append(m1)

    library['movies'].sort(key=lambda x: x.Title)
    library['cds'].sort(key=lambda x: x.Album)
    library['books'].sort(key=lambda x: x.Title)


def update_cd_prices(cds):
    # [c, c2, c3, c, c2]
    # [0,  1,  2, 3,  4]
    for i in range(len(cds)):
        count = 1
        for j in range(len(cds)):
            if i == j:
                continue
            else:
                if (cds[i] == cds[j]):
                    count+=1
        cds[i].calculate_new_price(count)


def menu(library):
    done = False
    while not done:
        print()
        print("[1] Add a Book.")
        print("[2] Add a Movie.")
        print("[3] Add a CD.")
        print("[4] List Books in library.")
        print("[5] List Movies in library")
        print("[6] List CD in library")
        print("[7] List All Media in library")
        print("[8] Save to library")
        print("[9] Load library")
        print("[0] Exit the program.")

        option = int(input("Enter your option: "))

        if option == 0:
            done = True

        elif option == 1:         #gör option 1 add book
            Title = input("Title: ")
            Writer = input("Writer: ")
            Pages = int(input("Pages: "))
            Purchase_Price = int(input("Purchase Price: "))
            Purchasing_Year = int(input("Purchasing Year: "))         
            Book = Books(Title, Writer, Pages, Purchase_Price, Purchasing_Year)
            library['books'].append(Book)
            library['books'].sort(key=lambda x: x.Title)

        elif option == 2:       #gör option 2 add movie
            Title = input("Title: ")
            Director = input("Director: ")
            Length = int(input("Length: "))
            Purchase_Price = int(input("Purchase Price: "))
            Purchasing_Year = int(input("Purchasing Year: "))
            Movie = Movies(Title, Director, Length, Purchase_Price, Purchasing_Year)
            library['movies'].append(Movie)
            library['movies'].sort(key=lambda x: x.Title)

        elif option == 3:       #gör option 3 add CD
            Album = input("Album: ")
            Artist = input("Artist: ")
            Tracks = int(input("Tracks: "))
            Purchase_Price = int(input("Purchase Price: "))
            Purchasing_Year = int(input("Purchasing Year: "))
            CD = CD(Album, Artist, Tracks, Purchase_Price, Purchasing_Year)
            library['cds'].append(CD)
            library['cds'].sort(key=lambda x: x.Album)
            update_cd_prices(library['cds'])

        elif option == 4:       #gör option 4 show all books]
            print(library['books'])   

        elif option == 5:       #gör option 5 show all movies
            print(library['movies'])

        elif option == 6:       #gör option 6 show all CD
            print(library['cds'])

        elif option == 7:       #gör option 7 show full library
            for k, v in library.items():
                print(f'{k}: {v}\n')

        elif option == 8:       #Save data
            with open("Save_library.pick", "wb") as Save_data:
                pickle.dump(library, Save_data)
                print("Library saved!")

        elif option == 9:       #Load Data
            with open("Save_library.pick", "rb") as Load_data:
                library = pickle.load(Load_data)
                print(library)

        else:
            print("Invalid option.")
    print("Thanks for using the library. Goodbye!")


def main():
    # set up library
    library = {}
    library['books'] = []
    library['movies'] = []
    library['cds'] = []
    create_standard_library(library)
    update_cd_prices(library['cds'])

    # call main program
    menu(library)

if __name__ == '__main__':
    main()