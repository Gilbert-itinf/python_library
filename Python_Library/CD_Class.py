class CD:
    def __init__(self, Album, Artist, Tracks, Purchase_Price, Purchasing_Year):
        self.Album = Album
        self.Artist = Artist
        self.Tracks = Tracks
        self.Purchase_Price = Purchase_Price
        self.Purchasing_Year = Purchasing_Year
        self.New_Price = None

    def __repr__(self):
        return self.New_Price_CD()

    def __str__(self):
        return self.New_Price_CD()

    def __eq__(self, other):
        return (self.Artist == other.Artist) & (self.Album == other.Album)

    def Show_Info_CD(self):
        return(f"{{Album: {self.Album}, Artist: {self.Artist}, Tracks: {self.Tracks}, Purchase Price: {self.Purchase_Price}kr, Year of Purchase: {self.Purchasing_Year}}}") 

    def calculate_new_price(self, count):
        self.New_Price = int(self.Purchase_Price) / count

    def New_Price_CD(self):
        if self.New_Price is None:
            return self.Show_Info_CD()
        else:
            return(f"{{Album: {self.Album}, Artist: {self.Artist}, Tracks: {self.Tracks}, New Price: {self.New_Price}kr, Year of Purchase: {self.Purchasing_Year}}}")
