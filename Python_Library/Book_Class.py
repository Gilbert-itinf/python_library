class Books:
    def __init__(self, Title, Writer, Pages, Purchase_Price, Purchasing_Year):
      self.Title = Title
      self.Writer = Writer
      self.Pages = Pages
      self.Purchase_Price = Purchase_Price
      self.Purchasing_Year = Purchasing_Year

    def __repr__(self):
        return self.New_Price_Books()

    def __str__(self):
        return self.New_Price_Books()   

    def Show_Info_Book(self):
        return(f"{{Title: {self.Title}, Writer: {self.Writer}, Pages: {self.Pages}, New Price: {self.Purchase_Price}kr, Year of Purchase: {self.Purchasing_Year}}}")

    def New_Price_Books(self):
        if 2021 - int(self.Purchasing_Year) <= 50:
            Year =2021 - int(self.Purchasing_Year)
            Discount = .9 ** Year
            (New_Price) = Discount * float(self.Purchase_Price)
            return(f"{{Title: {self.Title}, Writer: {self.Writer}, Pages: {self.Pages}, New Price: {int(New_Price)}kr, Year of Purchase: {self.Purchasing_Year}}}")
            
        else:
            Increase = .9 ** 50
            Interest = Increase * self.Purchase_Price
            Year = 2021 - self.Purchasing_Year - 50
            Increase = 1.08 ** Year
            New_Price = Increase * Interest
            return(f"{{Title: {self.Title}, Writer: {self.Writer}, Pages: {self.Pages}, New Price: {int(New_Price)}kr, Year of Purchase: {self.Purchasing_Year}}}")
    

                

