import random

class Movies:

  def __init__(self, Title, Director, Length, Purchase_Price, Purchasing_Year):
      self.Title = Title
      self.Director = Director
      self.Length = Length
      self.Purchase_Price = Purchase_Price
      self.Purchasing_Year = Purchasing_Year

  def __repr__(self):
   return self.New_Price_Movies()

  def __str__(self):
    return self.New_Price_Movies()

  def Show_Info_Movie(self):
    return(f"{{Title: {self.Title}, Director: {self.Director}, Lenght: {self.Length}, Purchase Price: {self.Purchase_Price}kr, Year of Purchase: {self.Purchasing_Year}}}")  


  def New_Price_Movies(self):
    Condition = random.randint(1,10)
    Year = 2021 - int(self.Purchasing_Year)
    Discount = .9 ** Year
    New_Price = Discount * Condition / 10
    Final_Price = New_Price * int(self.Purchase_Price)
    return(f"{{Title: {self.Title}, Director: {self.Director}, Lenght: {self.Length}, Condition: {Condition}, New Price: {int(Final_Price)}kr, Year of Purchase: {self.Purchasing_Year}}}")

