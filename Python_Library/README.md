# itinf20_python_final

Info om Biblioteket:
Biblioteket är ett program som sparar 3 olika media (book,cd och movie). Utöver det så kan man som användare lägga till nya media och programet kommer att hjälpa dig att sortera och räkna ut ett nytt pris.

Hur Programet Körs:

När man startar programet så kommer man få se en meny med alternativ 1 till 9. 

Alternativ (1,2,3)
Dom första 3 alternativen lägger till antligen en bok,film eller cd beronde på vilken du väljer och kommer att lägga den i respektive lista in i biblioteket.

Alternativ (4,5,6)
Dessa alternativ kommer att sortera den valda classen i alfabetisk ordning beronde på title/album namn. Den kommer desutom kalla på en utav dom olika funktionerna för att räkna ut ett nytt pris. 

Alternativ 7 
Gör detsamma som 4,5,6 men den skriver ut alla 3 classer i ordningen Books, Movies och sist CD.

Alternativ (7,8)
Använder sig utav pickle för att spara och ladda filen "Save_library.pick".

Alternativ 9
Anvädes bara för att stänga av programet.

Källor:

pickle: https://docs.python.org/3/library/pickle.html
random: https://docs.python.org/3/library/random.html

Även fått några tips från en kompis. 




















## Get started

1. Create a fork of this project
2. Add robert-alfwar as a member
3. Clone your fork


##  Hand in

1. Create a merge request against this original repository
2. Download a zip or tar.gz of your project and upload in studentportalen
